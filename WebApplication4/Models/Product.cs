﻿namespace WebApplication4.Models
{
    public class Product
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Amount { get; set; }

        public int Price { get; set; }

    }
}