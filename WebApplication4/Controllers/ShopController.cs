﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication4.Models;

namespace WebApplication4.Controllers
{
    public class ShopController : ApiController
    {

        public static List<Product> Products = new List<Product>
        {
            new Product {Amount = 100, Id = 1 , Name = "Apple", Price = 20},
             new Product {Amount = 100, Id = 2 , Name = "Banana", Price = 25},
              new Product {Amount = 100, Id = 1 , Name = "Potato", Price = 5},
               new Product {Amount = 100, Id = 1 , Name = "Tomato", Price = 33},
                new Product {Amount = 100, Id = 1 , Name = "Milk", Price = 32},
        };
        private object pokupka;

        // GET: api/Shop
        public List<Product> Get()
        {
            return Products;
        }

        

        // GET: api/Shop/5
        public Product  Get(int id)
        {
            foreach (var product  in Products)
            {
                if (product.Id == id)
                {
                    return product;
                }
            }
            return null;
        }

        public Void Buy()
        {
            var items = new List<PurchaseItem>
            {
                new PurchaseItem{ Id = 5, Amount = 3},
                 new PurchaseItem{ Id = 2, Amount= 6},
            };

            foreach (var product in Products)
            {
                foreach (var purchase in items)
                {
                    if(purchase.Id == product.Id && product.Amount >= purchase.Amount)
                        {
                        product.Amount  -=   purchase.Amount;
                    }
                }
            }
        }

        // POST: api/Shop
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Shop/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Shop/5
        public void Delete(int id)
        {
        }
    }
}
